
function insertAt(array, index, ...elementsArray){
    // console.log(elementsArray);
    array.splice(index, 0, ...elementsArray);

}

var Array= [
    {
    "color":"black",
    "type":"small",
    "model":"van"
    },
    {
        "color":"red",
        "type":"small",
        "model":"car"
    },
    {
        "color":"green",
        "type":"large",
        "model":"van"
    },
    {
        "color":"white",
        "type":"medium",
        "model":"wagon"
    }
]

insertAt(Array, 1,{"color":"blue","type":"medium","model":"wagon"});
console.log(Array)