let cars = [
    {
        "color":"purple",
        "type":"van",
        "registration": new Date('2021-06-02'),
        "capacity": 7
    },
    {
        "color":"red",
        "type":"station wagon",
        "registration": new Date('2021-04-02'),
        "capacity": 5
    },
    {
        "color":"green",
        "type":"car",
        "registration": new Date('2021-06-02'),
        "capacity": 2
    },
    {
        "color":"yellow",
        "type":"minicar",
        "registration": new Date('2021-08-05'),
        "capacity": 3
    },
    {
        "color":"gray",
        "type":"minivan",
        "registration": new Date('2021-11-02'),
        "capacity": 6
    },
    {
        "color":"purple",
        "type":"van",
        "registration": new Date('2021-06-02'),
        "capacity": 7
    }
]
console.log(cars);

var vhevk = new Array();
console.log(vhevk);

let car = {
    "color": "red",
    "type": "marcidies",
    "registration": new Date ('2020-05-04'),
    "capacity":2
}
cars.unshift(car);

let car1 = {
    "color": "brown",
    "type": "marcidies",
    "registration": new Date ('2021-05-04'),
    "capacity":2
}
cars.push(car);

let car2 = {
    "color": "brown",
    "type": "marcidies",
    "registration": new Date ('2021-05-04'),
    "capacity":2
}        

let car5 = cars.find(car => car.color === "red" && car.type === "mercidies");
console.log (car5);

let redCars = cars.filter(car => car.color ==="red");
console.log (redCars);

let sizes = cars.map(car => {
    if (car.capacity <=3){
        return "small";
    }
    if (car.capcity <=5){
        return "medium";
    }
    return "large"; 
})

// Test
// return cars with small larg media with their count example small 3 , large 1, medium 2
// return cars with new additional property

let carsProperties = cars.map (car => {
    let properties ={
        "capacity": car.capacity,
        "size": "large"
    }
    if (car.capacity <=3){
        properties['size']="small";
    }
    if (car.capacity <=5){
        properties['size']="medium";
    }
    return properties;
})
console.log(carsProperties);
console.log(cars);

cars.forEach(car=>{
    car['size']="large";
    if(car.capacity<=5){
        car['size']="medium";
    }
    if(car.capacity<=3){
        car['size']="small";
    }
})
console.log(cars);

var sortingCars = cars.map(car=>{
return {
    'capacity':car.capacity,
    'status':(car.capacity > 4 ? true: false)
};
}
);
const orderedcars1 = sortingCars.sort((c3,c4)=>(c3.capacity > c4.capacity) ? 1 : (c3.capacity<c4.capacity)? -1 :0);
console.log('Ascending');
console.log(orderedcars1);

let orderedcars = cars.sort((c1,c2)=>(c1.capacity < c2.capacity) ? 1 : (c1.capacity>c2.capacity)? -1 :0);
console.log(orderedcars);


let carsSome=cars.some (car =>car.color==="red" && car.type==="station wagon");
console.log('someFunction');
console.log(carsSome);

let carsEvery=cars.every (car=>car.capacity>=4);
console.log(carsEvery);
